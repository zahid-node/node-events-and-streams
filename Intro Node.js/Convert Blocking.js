var fs = require('fs');

//Blocking code
// var contents = fs.readFileSync('index.html');
// console.log(contents);

//Non Blocking code equivalent of above code
fs.readFile('index.html', function (err, contents) {
    console.log(contents);
});