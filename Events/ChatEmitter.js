// We're going to create a custom chat EventEmitter.

// Create a new EventEmitter object and assign it to a variable called 'chat'.
var events = require('events');
var EventEmitter = events.EventEmitter;
var chat = new EventEmitter();

// Next, let's listen for the 'message' event on our new chat object. 
//Remember to add a callback that accepts the message parameter.
//Log the message to the console using console.log().
chat.on('message',(message)=>{
    console.log(message);
})


